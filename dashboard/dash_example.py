import requests
import json

def make_request():
    url = "http://192.168.0.132:5080/WeatherForecast"
    response = requests.get(url)
    response_json = response.json()
    print(response_json)

make_request()

'''
Dashboard
Example from 
https://dash.plotly.com/dash-daq
https://dash.plotly.com/dash-core-components/interval
'''
from dash import Dash, html, dcc, Input, Output, callback
import dash_daq as daq

app = Dash(__name__)

app.layout = html.Div([
    daq.Thermometer(
        id='my-thermometer-1',
        value=5,
        min=0,
        max=10,
        style={
            'margin-bottom': '5%',
            'margin-left': '5%',
            'display': 'inline-block'
        }
    ),
    daq.Thermometer(
        id='my-thermometer-2',
        value=5,
        min=0,
        max=10,
        style={
            'margin-bottom': '5%',
            'margin-left': '20%',
            'display': 'inline-block'
        }
    ),
    daq.Thermometer(
        id='my-thermometer-3',
        value=5,
        min=0,
        max=10,
        style={
            'margin-bottom': '5%',
            'margin-left': '20%',
            'display': 'inline-block'
        }
    ),
    dcc.Interval(
        id='interval-component',
        interval=5*1000, # in milliseconds
        n_intervals=0
    ),
])

value=1
@callback(
    Output('my-thermometer-1', 'value'),
    Output('my-thermometer-2', 'value'),
    Output('my-thermometer-3', 'value'),
    Input('interval-component', 'n_intervals')
)
def update_thermometer(value):  
    return (value+1,value+2,value)


if __name__ == '__main__':
    app.run(debug=True,host = '0.0.0.0')