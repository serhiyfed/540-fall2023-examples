'''
Example from https://youtu.be/A97QLHAqNuw?si=q53UWIOyDmntsstf

Test: Run the program and interrupt the program with Control+C
The program will manage the interruption SIGINT received by Control+C
Use this example to better manage the KeyboardInterrupt exeption given in the Tutorial.pdf
'''
import random
import signal
import threading
import time

exit_event = threading.Event()

def bg_thread():
    for i in range(1, 30):
        print(f'{i} of 30 iterations...')
        time.sleep(random.random())  # do some work...

        if exit_event.is_set():
            break

    print(f'{i} iterations completed before exiting.')

def signal_handler(signum, frame):
    exit_event.set()

signal.signal(signal.SIGINT, signal_handler)
th = threading.Thread(target=bg_thread)
th.start()
th.join()